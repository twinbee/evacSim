EvacSim 0.1

Requirements:

microsoft windows XP, Vista
2 GB RAM
2 GHz or higher processor
3d-accelerated graphics
2 GB disk space


Version history: (odd versions are development versions)

0.1 Initial release features:

explosions
basic scripting
multiple cameras
OBJ loader for navmeshes exported from Google Sketchup
OBJ loader for 3d decorative models exported from Google Sketchup
vision occlusion
cylindrical physical approximation of bodies
box physical approximation of walls
physical accuracy levels
rendering to jpeg
video post-compiling (with VirtualDub)
near-real time mode for 
